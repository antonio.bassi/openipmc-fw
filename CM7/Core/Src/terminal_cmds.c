/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/


#include "main.h"
#include "terminal.h"


#include "string.h"
#include "printf.h"
#include "mt_printf.h"
#include "ipmb_0.h"
#include "head_commit_sha1.h"
#include "fru_state_machine.h"
#include "device_id.h"
#include "tftp_client.h"
#include "write_bin_stmflash.h"
#include "bootloader_tools.h"
#include "network_ctrls.h"
#include "network_ctrls.h"
#include "lwip.h"



extern bool mt_CLI_GetIntState();

// Defined in lwip.c file. Used here to access network resources.
extern struct netif gnetif;


/*
 * Command "info"
 *
 * Prints general information about IPMC
 */
#define CMD_INFO_NAME "info"
#define CMD_INFO_DESCRIPTION "Print information about this IPMC."
#define CMD_INFO_CALLBACK info_cb
static uint8_t info_cb()
{
	// Convert commit hash from hex to string.
	char commit_s[9];
	sprintf_(commit_s, "%08x", HEAD_COMMIT_SHA1);

	//Get MAC address
	uint8_t mac_addr[6];
	load_user_defined_mac_addr(mac_addr);

	//Get IP address, mask and gateway
	ip4_addr_t* ipaddr  = netif_ip4_addr   ( &gnetif );
	ip4_addr_t* netmask = netif_ip4_netmask( &gnetif );
	ip4_addr_t* gw      = netif_ip4_gw     ( &gnetif );

	mt_printf( "\r\n\n" );
	mt_printf( "OpenIPMC-HW\r\n" );
	mt_printf( "Firmware commit: %s\r\n", commit_s );
	mt_printf( "\r\n" );
	mt_printf( "Target Board: %s\r\n", ipmc_device_id.device_id_string );
	mt_printf( "IPMB-0 Addr: 0x%x\r\n", ipmb_0_addr );
	mt_printf( "\r\nNetwork:\r\n" );
	mt_printf( "MAC Addr: %02X:%02X:%02X:%02X:%02X:%02X\r\n", mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5] );
	mt_printf( " IP Addr: %d.%d.%d.%d\r\n",  ipaddr->addr&0xFF, ( ipaddr->addr>>8)&0xFF, ( ipaddr->addr>>16)&0xFF, ( ipaddr->addr>>24)&0xFF);
	mt_printf( " netmask: %d.%d.%d.%d\r\n", netmask->addr&0xFF, (netmask->addr>>8)&0xFF, (netmask->addr>>16)&0xFF, (netmask->addr>>24)&0xFF);
	mt_printf( " GW Addr: %d.%d.%d.%d\r\n",      gw->addr&0xFF, (     gw->addr>>8)&0xFF, (     gw->addr>>16)&0xFF, (     gw->addr>>24)&0xFF);



	return TE_OK;
}


/*
 * Command "handle"
 *
 * Force a state change for ATCA Face Plate Handle
 */
#define CMD_ATCA_HANDLE_NAME "handle"
#define CMD_ATCA_HANDLE_DESCRIPTION "\
Force a state change for ATCA Face Plate Handle.\r\n\
\t[ o | c ] for Open or Close."
#define CMD_ATCA_HANDLE_CALLBACK atca_handle_cb
static uint8_t atca_handle_cb()
{
	fru_transition_t fru_trigg_val;

	if ( CLI_IsArgFlag("-o") )
	{
		fru_trigg_val = OPEN_HANDLE;
		xQueueSendToBack(queue_fru_transitions, &fru_trigg_val, 0UL);
	}
	else if ( CLI_IsArgFlag("-c") ){
		fru_trigg_val = CLOSE_HANDLE;
		xQueueSendToBack(queue_fru_transitions, &fru_trigg_val, 0UL);
	}

	return TE_OK;
}


/*
 * Command "debug-ipmi"
 *
 * Enable to show the IPMI messaging from OpenIPMC
 */
#define CMD_DEBUG_IPMI_NAME "debug-ipmi"
#define CMD_DEBUG_IPMI_DESCRIPTION "\
Enable to show the IPMI messaging from OpenIPMC"
#define CMD_DEBUG_IPMI_CALLBACK debug_ipmi_cb
extern int enable_ipmi_printouts;
static uint8_t debug_ipmi_cb()
{
	enable_ipmi_printouts = 1; // Enable Debug

	while(1)
	{
		// Wait for ESC
		vTaskDelay( pdMS_TO_TICKS( 500 ) );
		if( mt_CLI_GetIntState() ){
			enable_ipmi_printouts = 0; // Disable debug
			break;
		}
	}

	return TE_WorkInt;
}


/*
 * Command "load-bin"
 */
#define CMD_LOAD_BIN_NAME "load-bin"
#define CMD_LOAD_BIN_DESCRIPTION "\
Load binary from a TFTP server into TEMP area (Sector 12).\r\n\
\t\tex: load-bin 192 168 0 1 new_firmware.bin\r\n\
\t\tThis client always load file named fimware.bin"
#define CMD_LOAD_BIN_CALLBACK load_bin_cb

_Bool tftp_impl_fwupload_start( const ip_addr_t *server_addr, const char* fname );
_Bool tftp_impl_fwupload_running( void );
void  tftp_impl_fwupload_abort( void );

uint8_t load_bin_cb()
{
	// Get Server IP addr from parameters
	ip_addr_t tfpt_server_addr;
	IP_ADDR4(&tfpt_server_addr, CLI_GetArgDec(0)&0xFF, CLI_GetArgDec(1)&0xFF, CLI_GetArgDec(2)&0xFF, CLI_GetArgDec(3)&0xFF);

	// Get filename from parameters
	char* filename = CLI_GetArgv()[5];

	mt_printf( "\r\n" );

	if( tftp_impl_fwupload_start( &tfpt_server_addr, filename ) )
	{
		while( tftp_impl_fwupload_running() )
		{
			vTaskDelay( pdMS_TO_TICKS( 500 ) );
			if( mt_CLI_GetIntState() ) // If ESC is pressed
			{
				tftp_impl_fwupload_abort();
				mt_printf( "Transfer Aborted!" );
				return TE_OK;
			}
		}
	}
	else
		mt_printf( "Transfer Failed!" );


	return TE_OK;
}


/*
 * Command "check-bin"
 */
#define CMD_CHECK_BIN_NAME "check-bin"
#define CMD_CHECK_BIN_DESCRIPTION "\
Check the validity of binary present in Sector 12."
#define CMD_CHECK_BIN_CALLBACK check_bin_cb
static uint8_t check_bin_cb()
{
	uint32_t crc;
	int is_valid = bin_stmflash_validate(9, &crc);
	mt_printf( "\r\n");
	if( is_valid == 0 )
		mt_printf( "Binary is invalid!\r\n" );
	mt_printf( "CRC: %x\r\n", crc );

	return TE_OK;
}


/*
 * Command "bootloader"
 */
#define CMD_BOOT_NAME "bootloader"
#define CMD_BOOT_DESCRIPTION "\
Manage Bootloader.\r\n\
\t\tNo argument: Print Bootloader status\r\n\
\t\t     enable: Bootloader boots first after reset\r\n\
\t\t    disable: OpenIPMC-FW boots directly after reset"
#define CMD_BOOT_CALLBACK bootloader_cb
static uint8_t bootloader_cb()
{
	uint8_t major_ver; // (version info not used here)
	uint8_t minor_ver;
	uint8_t aux_ver[4];

	mt_printf( "\r\n\r\n" );

	if ( CLI_IsArgFlag("enable") )
	{
		if( bootloader_enable() == false )
			mt_printf( "Enabling failed!\r\n" );
	}
	else if( CLI_IsArgFlag("disable") )
	{
		if( bootloader_disable() == false )
			mt_printf( "Disabling failed!\r\n" );
	}

	mt_printf( "Bootloader is present in the Flash: " );
	if( bootloader_is_present( &major_ver, &minor_ver, aux_ver ) )
		mt_printf( "YES  ver:%d.%d.%d  %02x%02x%02x%02x\r\n", major_ver, (minor_ver>>4)&0x0F, (minor_ver)&0x0F, aux_ver[0], aux_ver[1], aux_ver[2], aux_ver[3] );
	else
		mt_printf( "NO\r\n" );

	mt_printf( "Run bootloader on boot is enabled: " );
	if( bootloader_is_active() )
		mt_printf( "YES\r\n" );
	else
		mt_printf( "NO\r\n" );

	return TE_OK;
}



#define CMD_DHCP_NAME "dhcp"
#define CMD_DHCP_DESCRIPTION "\
Manage DHCP.\r\n\
\t\t         on: Enable DHCP\r\n\
\t\t        off: Disable DHCP"
#define CMD_DHCP_CALLBACK dhcp_cb
static uint8_t dhcp_cb()
{
	if ( CLI_IsArgFlag("on") )
	{
		eth_ctrls_dhcp_enable();
	}
	else if ( CLI_IsArgFlag("off") )
	{
		eth_ctrls_dhcp_disable();
	}
	else
	{
		mt_printf( "Invalid argument\r\n" );
	}

	return TE_OK;
}



#define CMD_SETIP_NAME "setip"
#define CMD_SETIP_DESCRIPTION "\
Set IP address DHCP.\r\n\
\t\t        Set the IP address, netmask and gateway\r\n\
\t\t         eg: setip 192.168.0.123 255.255.255.0 192.168.0.1"
#define CMD_SETIP_CALLBACK setip_cb
static uint8_t setip_cb()
{

	uint8_t ip[4], mask[4], gw[4];


	if( eth_ctrls_parse_ip_addr ( CLI_GetArgv()[1], ip   ) != 0 )
		return TE_ArgErr;
	if( eth_ctrls_parse_ip_addr ( CLI_GetArgv()[2], mask ) != 0 )
		return TE_ArgErr;
	if( eth_ctrls_parse_ip_addr ( CLI_GetArgv()[3], gw   ) != 0 )
		return TE_ArgErr;

	eth_ctrls_change_ip_addr(   ip[0],   ip[1],   ip[2],   ip[3],
	                          mask[0], mask[1], mask[2], mask[3],
	                            gw[0],   gw[1],   gw[2],   gw[3]  );

	return TE_OK;
}



/*
 * Command "reset"
 *
 * It replaces the native command "~"
 */
#define CMD_RESET_NAME "reset"
#define CMD_RESET_DESCRIPTION "\
Reset IPMC. Use this instead \"~\"!"
#define CMD_RESET_CALLBACK reset_cb
static uint8_t reset_cb()
{
	NVIC_SystemReset();
}




/*
 * Add commands to the CLI
 */
void add_terminal_commands( void )
{
	CLI_AddCmd( CMD_INFO_NAME,        CMD_INFO_CALLBACK,        0, 0, CMD_INFO_DESCRIPTION        );
	CLI_AddCmd( CMD_ATCA_HANDLE_NAME, CMD_ATCA_HANDLE_CALLBACK, 1, 0, CMD_ATCA_HANDLE_DESCRIPTION );
	CLI_AddCmd( CMD_DEBUG_IPMI_NAME,  CMD_DEBUG_IPMI_CALLBACK,  0, 0, CMD_DEBUG_IPMI_DESCRIPTION  );
	//CLI_AddCmd( CMD_LOAD_BIN_NAME,    CMD_LOAD_BIN_CALLBACK,    5, TMC_None, CMD_LOAD_BIN_DESCRIPTION    );
	//CLI_AddCmd( CMD_CHECK_BIN_NAME,   CMD_CHECK_BIN_CALLBACK,   0, TMC_None, CMD_CHECK_BIN_DESCRIPTION   );
	CLI_AddCmd( CMD_BOOT_NAME,        CMD_BOOT_CALLBACK,        0, TMC_None, CMD_BOOT_DESCRIPTION   );
	CLI_AddCmd( CMD_DHCP_NAME,        CMD_DHCP_CALLBACK,        1, TMC_None, CMD_DHCP_DESCRIPTION   );
	CLI_AddCmd( CMD_SETIP_NAME,       CMD_SETIP_CALLBACK,       3, TMC_None, CMD_SETIP_DESCRIPTION  );



	CLI_AddCmd( CMD_RESET_NAME,       CMD_RESET_CALLBACK,       0, TMC_None, CMD_RESET_DESCRIPTION   );
}
