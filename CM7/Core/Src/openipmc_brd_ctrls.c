
/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/

/*
 * This file contains code which are used by OpenIPMC to perform operations over
 * the ATCA board "payload".
 */

#include <dimm_gpios.h>
#include "cmsis_os.h"
#include <stdbool.h>

//OpenIPMC includes
#include "power_manager.h"
#include "sdr_definitions.h"

#define __weak __attribute__((weak))

struct
{
	
	_Bool   status;
	uint8_t current_power_level;
	uint8_t new_power_level;
	
} benchtop_mode = {false, 0, 0};


extern void board_specific_activation_policy( uint8_t current_power_level, uint8_t new_power_level );


/*
 * Switch Power Level
 *
 * This functions is called by OpenIPMC when a Power Level change is requested by
 * Shelf Manager
 */
void ipmc_pwr_switch_power_level_on_payload( uint8_t new_power_level )
{
	// Benchtop mode prevents actions from Shelf Manager
	if( benchtop_mode.status == true )
		return;
	
	uint8_t current_power_level = ipc_pwr_get_current_power_level();
	board_specific_activation_policy( current_power_level, new_power_level );
}


/*
 * Board Specific routine for changing the Power Level
 * 
 * A minimal implementation is presented here as "weak" function.
 * This function must be redefined for board-specific needs.
 */
__weak void board_specific_activation_policy( uint8_t current_power_level, uint8_t new_power_level )
{
	
	/*
	 * For customization, 'current_power_level' and 'new_power_level' can be used to improve
	 * any the transition between power levels.
	 */
	
	
	// DEACTIVATION
	if( new_power_level == 0)
	{
		
		// Customize DEACTIVATION process
		
		/*
		 * Put here any code related to deactivation of the board.
		 * E.g: Shut down Linux
		 */
		
		// Switch off the 12V rail for payload
		EN_12V_SET_STATE(RESET);
		
	}
	// ACTIVATION
	else 
	{
		
		// Switch on the 12V rail for payload
		EN_12V_SET_STATE(SET);
		
		// Customize ACTIVATION process
		
		/*
		 * Put here any code related to activation of the board.
		 * E.g: Boot Linux
		 */
		
	}
	
	return;
}



/*
 * Force a new power level in benchtop mode
 * 
 * In this mode, Shelf Manager looses the ability to change the Power Level
 */
void set_benchtop_payload_power_level( uint8_t new_power_level )
{
	benchtop_mode.status == true;
	
	board_specific_activation_policy( benchtop_mode.current_power_level, new_power_level );
	
	benchtop_mode.current_power_level = new_power_level;
}



/*
 * Payload Cold Reset
 *
 * This functions is called by OpenIPMC when a Cold Reset command is received
 * from Shelf Manager
 */
void payload_cold_reset (void)
{

	// Typical operation over PAYLOAD_RESET signal on DIMM
	PAYLOAD_RESET_SET_STATE(RESET);
	osDelay(10); // Holds PAYLOAD_RESET Low for 10ms
	PAYLOAD_RESET_SET_STATE(SET);

}



